﻿using System;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using Assert = NUnit.Framework.Assert;

namespace RestSharpPetStore
{
	//PetID, CategoryName, PetName, PetStatus, TagName, PhotoURL
	[TestFixture("201918", "DOG", "Zeus", "available", "HouseDog", "https://upload.wikimedia.org/wikipedia/en/1/12/Brian_Griffin.png")]
	public class PetStoreTests
	{
		private string petId;
		private string categoryName;
		private string petName;
		private string petStatus;
		private string tagName;
		private string photoURL;
		private string apiUrl = "https://petstore.swagger.io/v2/pet";

		public PetStoreTests(string petId, string categoryName, string petName, string petStatus, string tagName, string photoURL)
		{
			this.petId = petId;
			this.categoryName = categoryName;
			this.petName = petName;
			this.petStatus = petStatus;
			this.tagName = tagName;
			this.photoURL = photoURL;
		}

		[Test]
		public void CreatePet()
		{
			string jsonData = "{" +
										"\"id\": \"" + petId + "\"," +
										"\"category\": {" +
										"\"id\": \"13\"," +
										"\"name\": \"" + categoryName + "\"" +
										"}," +
										"\"name\": \"" + petName + "\"," +
										"\"photoUrls\": [" +
										"\"" + photoURL + "\"" +
										"]," +
										"\"tags\": [" +
										"{" +
										"\"id\": \"11\"," +
										"\"name\": \"" + tagName + "\"" +
										"}" +
										"]," +
										"\"Status\": \"" + petStatus + "\"" +
										"}";

			IRestClient restClient = new RestClient();
			IRestRequest request = new RestRequest()
			{
				Resource = apiUrl
			};

			request.AddHeader("Content-Type", "application/json");
			request.AddHeader("Accept", "application/json");
			request.RequestFormat = DataFormat.Json;
			request.AddJsonBody(jsonData);

			IRestResponse response = restClient.Post(request);
			Assert.AreEqual(200, (int)response.StatusCode);
			Console.WriteLine(response.Content);
		}

		[Test]
		public void CheckPetByIDCreated()
		{
			var client = new RestClient(apiUrl);
			var request = new RestRequest("/" + petId, Method.GET);

			var response = client.Execute(request);
			JObject obj = JObject.Parse(response.Content);
			
			Assert.AreEqual(200, (int)response.StatusCode);
			Assert.That(obj["name"].ToString(), Is.AnyOf(petName), "Pet Name is not as Expected!");
			Assert.That(obj["id"].ToString(), Is.AnyOf(petId), "Pet ID is not as Expected!");
			Assert.That(obj.SelectToken("category.name").ToString(), Is.AnyOf(categoryName), "Pet Category is not as Expected!");
			StringAssert.Contains(photoURL, obj.SelectToken("photoUrls").ToString(), "Photo URL is not as Expected!");
			Console.WriteLine(obj.ToString());
		}

		[Test]
		public void UpdatePet()
		{
			string jsonData = "{" +
										"\"id\": \"" + petId + "\"," +
										"\"category\": {" +
										"\"id\": \"13\"," +
										"\"name\": \"" + categoryName + "\"" +
										"}," +
										"\"name\": \"BrianUpdated\"," +
										"\"photoUrls\": [" +
										"\"" + photoURL + "\"" +
										"]," +
										"\"tags\": [" +
										"{" +
										"\"id\": \"11\"," +
										"\"name\": \"" + tagName + "\"" +
										"}" +
										"]," +
										"\"Status\": \"" + petStatus + "\"" +
										"}";

			IRestClient restClient = new RestClient();
			IRestRequest request = new RestRequest()
			{
				Resource = apiUrl
			};

			request.AddHeader("Content-Type", "application/json");
			request.AddHeader("Accept", "application/json");
			request.RequestFormat = DataFormat.Json;
			request.AddJsonBody(jsonData);

			IRestResponse response = restClient.Put(request);
			Assert.AreEqual(200, (int)response.StatusCode);
			Console.WriteLine(response.Content);
		}

		[Test]
		public void DeletePet()
		{
			var client = new RestClient(apiUrl);
			var request = new RestRequest("/" + petId, Method.DELETE);
			var response = client.Execute(request);
			Assert.AreEqual(200, (int)response.StatusCode);
			Console.WriteLine(response.ToString());
		}

		[Test]
		public void CheckPetByIDDeleted()
		{
			var client = new RestClient(apiUrl);
			var request = new RestRequest("/" + petId, Method.GET);
			var response = client.Execute(request);
			JObject obj = JObject.Parse(response.Content);
			
			Assert.AreEqual(404, (int)response.StatusCode);
			Assert.That(obj["message"].ToString(), Is.AnyOf("Pet not found"), "Message is not as Expected!");
			Console.WriteLine(obj.ToString());
		}

	}
}
