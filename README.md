﻿# Techincal Test - Pet Store 
This project was developed by Nikola Nikic as Technical Test for Ding

## Prerequisites

What things you need to install the software and how to install them

```
| Visual Studio 2019 |
RestSharp |
NUnit |
Newtonsoft.Json |
```

## Running the tests

After building the project, you can open Test explorer where you should be able to se following Tests:

```
1. CreatePet
```

```
2. CheckPetByIDCreated
```

```
3. UpdatePet
```

```
4. DeletePet
```

```
5. CheckPetByIDDeleted
```

Same could be found under class PetStoreTests.cs
You can run scenarios eather from Test Explorer or from class itself - Right Click on scenario -> Run

### Test Data
Test Data is placed under NUnit TestFixture attribute - PetStoreTests.cs line: 10
NOTE: Name for Update Scenario is hardcoded to BrianUpdated